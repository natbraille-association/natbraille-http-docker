# get a mongo realm database json dump 

## connect to distant server

    ssh -o PubkeyAuthentication=no  <username>@dsi-liris-natb.univ-lyon1.fr

`-o PubkeyAuthentication=no` not needed ; for not using local keys.

## produce json dump of mongo realm database

    mkdir dump.yyyy.mm.dd

    cd dump.yyyy.mm.dd

    mongoexport --db natbrailleRealm --collection user --jsonArray --out natbrailleRealm.json

## retrieve the dump from the server

### for instance using sshfs 

    # mount
    apt install sshfs

    mkdir /path/to/home/local-mountpoint
    sshfs <username>@dsi-liris-natb.univ-lyon1.fr:/home/<username> /path/to/home/local-mountpoint

    # copy
    cp /path/to/home/local-mountpoint/natbrailleRealm.json ./files/mongodb-user-json-dump/natbrailleRealm_user.json

    # umount
    fusermount -u /path/to/home/local/mountpoint
    
mongo realm json dump is in `./files/mongodb-user-json-dump/natbrailleRealm_user.json`

# transform the dump to csv

    apt install jq

    jq -r '["user_name", "user_pass"], (.[] | [.userName, .password]) | @csv ' ./files/mongodb-user-json-dump/natbrailleRealm_user.json > ./files/mongodb-user-json-dump/realm_user_for_postgres_import.csv

mongo realm csv is in `./files/mongodb-user-json-dump/realm_user_for_postgres_import.csv`

# import csv dump in postgres container `tomcatrealm` database

container must be started if not already up

    sudo docker compose up

postgres formated dump is  `./files/mongodb-user-json-dump/realm_user_for_postgres_import.csv`

which in postgres container is `/mongodb-user-json-dump/realm_user_for_postgres_import.csv`

    # get container id
    sudo docker container ls | grep 'docker-postgres' #| awk '{print $1}'

    # Import the data into the table
    sudo docker container exec <containerid> psql -d tomcatrealm -U postgres  -c "COPY users(user_name, user_pass) FROM '/mongodb-user-json-dump/realm_user_for_postgres_import.csv' DELIMITER ',' CSV HEADER;"

    # set user_role for user having none
    sudo docker container exec <containerid> psql -d tomcatrealm -U postgres  -c "insert into user_roles (user_name,role_name) select user_name, 'user' from users where user_name not in (select user_name from user_roles);"
