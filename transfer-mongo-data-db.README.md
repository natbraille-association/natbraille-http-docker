# make a mongo content dump on the server

    ssh ...
    mkdir my-dump
    cd my-dump  
    mongodump 
    tar cvzg dump.2022.12.10.tgz dump/

# retrieve it from the server

retrieve `dump.2022.12.10.tgz` from server

(see migrate-realm-db.README.txt )

## put the dump in the docker compose volume

the directory volume is the one defined in `docker-compose.yml` as `./files/mongodbdump:/natbraille-dump/`    

    cp /path/to/dump.2022.12.10.tgz ./files/mongodbdump

# start docker compose

    docker compose up

(or just the mongo docker)

# bash to mongo container 

## get a shell

    sudo docker container exec -it $(sudo docker container ls | grep 'mongo:4.4.18-focal' | awk '{print $1}') bash

## uncompress dump

    tar xvzf natbraille-dump/dump.2022.12.10.tgz 

## restore

    mongorestore dump

## check 
  
open a mongo shell

  mongo

in mongo shell 

    use natbrailleRealm
    db.user.find({})
    ...

# remove  ?

    mongo

    use db <some-db-name>;
    db.dropDatabase();

## create user (NO ANYMORE IS NOT THE REALM ANYMORE)

    mongo

    use db natbrailleRealm
    newUser = { "userName" : "joe", "password" : "joe", "roles" : [{ "_id" : ObjectId(), "name" : "user"}] };
    db.user.insert(newUser);