#!/bin/sh

USERNAME=$1;
PASSWORD=$2;

#
# check command parameters
#

if  [ -e $USERNAME ] ; then
    echo "username ?"
    echo "usage : addNatbrailleUser.bash username password" ; 
    exit;  
fi;
if  [ -e $PASSWORD ] ; then
    echo "password ?"
    echo "usage : addNatbrailleUser.bash username password" ; 
    exit;  
fi;

#
# insert if not exist
#

REALM_SQL_COMMAND="psql -d tomcatrealm -U postgres -c"

SQL_INSERT_USER="insert into users (user_name, user_pass) values ('$USERNAME','$PASSWORD') ;"
SQL_INSERT_USER_ROLE="insert into user_roles (user_name, role_name) values ('$USERNAME','user') ;"

$REALM_SQL_COMMAND "$SQL_INSERT_USER" 
$REALM_SQL_COMMAND "$SQL_INSERT_USER_ROLE" 
