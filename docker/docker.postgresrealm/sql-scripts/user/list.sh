#!/bin/sh


echo '
use natbrailleRealm 
db.user.find().toArray()
' | mongo | grep userName | sed 's/\s*"userName"\s*:\s*"\(.*\)",*/\1/'


REALM_SQL_COMMAND="psql -d tomcatrealm -U postgres -c"

$REALM_SQL_COMMAND "select * from users"
