#!/bin/sh

USERNAME=$1;


#
# check command parameters
#

if  [ -e $USERNAME ] ; then
    echo "username ?"
    echo "usage : removeNatbrailleUser.bash username" ; 
    exit;  
fi;

REALM_SQL_COMMAND="psql -d tomcatrealm -U postgres -c"

SQL_DELETE_USER="delete from users where user_name='$USERNAME';"
SQL_DELETE_USER_ROLE="delete from user_roles where user_name='$USERNAME';"

$REALM_SQL_COMMAND "$SQL_DELETE_USER" 
$REALM_SQL_COMMAND "$SQL_DELETE_USER_ROLE"
