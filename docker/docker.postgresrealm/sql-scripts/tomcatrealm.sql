/* 
su - postgres ; 
createdb tomcatrealm 
*/
create table users
(
  user_name varchar(255) not null primary key,
  user_pass varchar(255) not null
);


create table roles
(
  role_name varchar(255) not null primary key
);

create table user_roles
(
  user_name varchar(255) not null,
  role_name varchar(255) not null,
  primary key( user_name, role_name )
);

/*
insert into roles (role_name) values ('user');
insert into users (user_name, user_pass) values ('vivien','xxxxx');
insert into user_roles (user_name, role_name) values ('vivien','user');
*/