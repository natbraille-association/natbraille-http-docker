# How-to

This docker is composed of
- a configured tomcat server hosting `natbraille-http.war`
- a postgres database for tomcat realm (natbraille-http users)
- a mongodb database for user documents

## startup

    cd docker
    docker compose up

## setup 

The application war, `natbraille-http.war` must be placed in `./docker/webapps/` dir which is mounted as the tomcat webapp volume.

alternatively, the `./docker/webapps/` volume line can be removed from `./docker/docker.compose.yml` and the war can be copied in the container by uncommenting the appropriate line in `./docker/Dockerfile`.

## configuration

- The natbraille server configuration is in `docker/files/etc_natbrailled`
- Natbraille default transcription configurations in `docker/files/usr_share_natbraille_options_server/*`

## add/list/remove user

email is used as user name

### add

    sudo subscriptions/addNatbrailleUser.bash <email> <password>
    subscriptions/telluser.bash <email> <password>

### list

    subscriptions/listNatbrailleUsers.bash 

### remove

    subscriptions/removeNatbrailleUser.bash <email>

## users and docments import

see `docker-compose.README.md` for docker commands

see `transfer-mongo-data-db.README.md` to import an existing document base

see `migrate-realm-db.README.md` to import an existing user realm

## persistence

Tomcat user realm and user documents data are persisted as volumes, resp. in `./docker/mongo-db` and `./docker/postgres-db`