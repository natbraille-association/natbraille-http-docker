#!/bin/sh

REALM_CONTAINER_ID=$(sudo docker container ls | grep 'docker-postgres' | awk '{print $1}')
echo "found docker container $REALM_CONTAINER_ID"

sudo docker container exec -it "$REALM_CONTAINER_ID" sql-scripts/user/remove.sh "$@"
