# 
# ./telluser.bash USERNAME PASSWORD [EMAIL]
# EMAIL is USERNAME if not provided
#
USERNAME=$1
PASSWORD=$2
EMAIL=$3
SUBJECT="Identifiants de connexion au serveur Natbraille"
#REPLYTO=vivien.guillet@laposte.net
REPLYTO=nat-users@listes.univ-lyon1.fr

if [ -n "$USERNAME" ] ; then
    
    if [ -z "$EMAIL" ] ; then
	echo warning $USERNAME as email - specify as third param if different
	EMAIL=$USERNAME
    fi
    if [ -n "$PASSWORD" ] ; then
	echo sending mail to $EMAIL with password $PASSWORD

	cat message.txt | sed s/USERNAME/$USERNAME/ | sed s/PASSWORD/$PASSWORD/ | mail -s "$SUBJECT" -r "$REPLYTO"  $EMAIL \
	    && echo $( date ) "$EMAIL" >> sent
    else
	echo error ! provide a password as second parameter
    fi
else
    echo error ! provide a username as first parameter
fi
