# using docker compose

    cd docker/

## build image

    sudo docker compose build

## run/stop image

    sudo docker compose up
    sudo docker compose down

# list everything

    echo "#image" ; sudo docker image ls -a ;\
    echo -e "\n#container" ; sudo docker container ls -a ;\
    echo -e "\n#compose" ; sudo docker compose ls -a ;\
    echo -e "\n#volume" ; sudo docker volume ls ;

# run commands in a container

## bash to a container

    sudo docker container exec -it <container-id> bash 

## psql to the postgres tomcat realm container

    sudo docker container exec <container-id> psql -d tomcatrealm -U postgres -c    

# remove everything docker
  
## stop composed & containers 

    sudo docker compose down
    sudo docker container rm $( sudo docker container ls -aq )
 
## remove images

    sudo docker image rm $( sudo docker image ls -aq )

## remove volumes
 
    sudo  docker volume rm $(sudo  docker volume ls -q)
